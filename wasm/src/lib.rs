#[macro_use]
extern crate getset;

mod architecture;
mod keys;
mod utils;

// When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
// allocator.
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

pub type Data = Vec<u8>;
pub use architecture::JSArchitecture;
pub use keys::Base64Keys;
