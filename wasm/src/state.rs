use std::{
    collections::HashMap,
    include_bytes,
};

use ppc::{
    Architecture,
    Identity,
    KeyQuartette,
    Message,
    Participant,
    Type,
};
use wasm_bindgen::prelude::*;

use crate::Data;

pub type OutlookKeyring = HashMap<Identity, KeyQuartette>;
pub type OutlookPossesionHandler = HashMap<Type, Vec<Message<Data>>>;
pub type OutlookParticipant =
    Participant<OutlookKeyring, OutlookPossesionHandler>;

fn init_architecture_bikestore() -> Architecture {
    let bytes = include_bytes!("bikestore.json");
    serde_json::from_slice(bytes).unwrap()
}

pub fn init_participant() -> OutlookParticipant {
    let architecture = init_architecture_bikestore();

    let mut keyring = HashMap::new();
    let mut rng = rand::thread_rng();
    for id in architecture.get_agents().keys() {
        keyring.insert(id.clone(), KeyQuartette::gen(&mut rng));
    }

    let possesion_handler: HashMap<Type, Vec<Message<Data>>> = HashMap::new();

    Participant::new(architecture, keyring, Some(possesion_handler))
}
