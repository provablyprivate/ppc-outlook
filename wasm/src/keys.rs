use ppc::KeyQuartette;
use rand::{
    CryptoRng,
    RngCore,
};
use wasm_bindgen::prelude::*;

use crate::Data;

#[derive(Getters)]
pub struct Keys<T> {
    #[get]
    verify_key: T,

    #[get]
    certify_key: T,

    #[get]
    sign_key: Option<T>,

    #[get]
    decertify_key: Option<T>,
}

impl<T> Keys<T> {
    pub fn make_public(&mut self) {
        self.sign_key = None;
        self.decertify_key = None;
    }

    pub fn map<U, F: Fn(T) -> U>(self, lambda: F) -> Keys<U> {
        Keys {
            verify_key: lambda(self.verify_key),
            certify_key: lambda(self.certify_key),
            sign_key: self.sign_key.map(&lambda),
            decertify_key: self.decertify_key.map(lambda),
        }
    }
}

impl Keys<Data> {
    pub fn gen<R>(rng: &mut R) -> Keys<Data>
    where
        R: RngCore + CryptoRng,
    {
        let key_quartette = KeyQuartette::gen(rng);
        Keys {
            verify_key: key_quartette.public_sign_data,
            certify_key: key_quartette.public_encryption_data,
            sign_key: Some(key_quartette.secret_sign_data),
            decertify_key: Some(key_quartette.secret_encryption_data),
        }
    }
}

#[wasm_bindgen]
pub struct Base64Keys(Keys<String>);

#[allow(non_snake_case)]
#[wasm_bindgen]
impl Base64Keys {
    pub fn generate() -> Base64Keys {
        Base64Keys(Keys::gen(&mut rand::thread_rng()).map(base64::encode))
    }

    pub fn getVerifyKey(&self) -> String { self.0.verify_key.clone() }

    pub fn getCertifyKey(&self) -> String { self.0.certify_key.clone() }

    pub fn getSignKey(&self) -> Option<String> { self.0.sign_key.clone() }

    pub fn getDecertifyKey(&self) -> Option<String> {
        self.0.decertify_key.clone()
    }

    pub fn makePublic(&mut self) { self.0.make_public(); }
}
