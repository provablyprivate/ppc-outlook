use ppc::{
    abstract_syntax::{
        abstract_syntax::ASPPC,
        eval::eval_as,
        typechecker::typecheck_as,
    },
    expand::expand_architecture,
};
use ppc_dsl::{
    self,
    ppc_dsl::get_as,
};
use wasm_bindgen::prelude::*;
use web_sys::console;

#[wasm_bindgen(js_name = Architecture)]
pub struct JSArchitecture();

#[allow(non_snake_case)]
#[wasm_bindgen(js_class = Architecture)]
impl JSArchitecture {
    pub fn fromDSL(text: String) -> JsValue {
        // TODO:
        //   This log message would be a killer macro!
        console::log_1(&JsValue::from(
            serde_json::to_string_pretty(&("DEBUG", ("text", &text))).unwrap(),
        ));

        let asppc: ASPPC = get_as(text);

        typecheck_as(&asppc);

        let (architecture, negative_constraints, positive_constraints) =
            eval_as(asppc);

        console::log_1(&JsValue::from(
            serde_json::to_string(&(
                "DEBUG",
                (
                    ("architecture", &architecture),
                    ("negative_constraints", &negative_constraints),
                    ("positive_constraints", &positive_constraints),
                ),
            ))
            .unwrap(),
        ));

        let expanded = expand_architecture(
            architecture,
            negative_constraints,
            positive_constraints,
        );

        console::log_1(&JsValue::from(
            serde_json::to_string(&("DEBUG", ("expanded", &expanded)))
                .unwrap(),
        ));

        JsValue::from_serde(&expanded).unwrap()
    }
}
