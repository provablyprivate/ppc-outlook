# ppc-outlook

**TODO:**
Write a good guide for how to deploy and develop the app.

## Requirements
* wasm-pack
* npm

## Run
*These steps has not been tested from a fresh build but should be something along these lines:*

### Installation
1. Install `npm` and `wasm-pack` if not yet on the system.

2. Run `npm install` to install all dependencies.

3. Run `npm run serve` to run the application. A text looking like this should appear:
```
  App running at:
  - Local:   https://localhost:3000/ 
  - Network: https://192.168.0.4:3000/
```

4.
    1. In Outlook, begin composing a new message.

    2. In the bottom toolbar (the same "row" as the *Send*-button) click the icon with three 
       horizontal dots.

    3. In the menu select *'Get Add-Ins'*. In the left menu click *'My add-ins'*.

    4. Scroll down to the bottom and click *'+ Add a custom add-in'*, then *'Add from file...'* and
       browse this folder up. Then select the file **manifest.xml** in the **manifest** directory.

    5. The next time you click the three horizontal dots *Provably Private Communications* should
       appear in the menu.

## Troubleshooting
Make sure the `npm run serve` is running while using the add-in. Everything is hosted from
*https://localhost:3000/*. Outlook more or less only stores the manifest file.
