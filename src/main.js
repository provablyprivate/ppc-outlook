import { createApp } from "vue";
import App from "./App.vue";

window.Office.initialize = () => {
  createApp(App).mount("#app");
};
